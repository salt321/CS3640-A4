# CS:3640 Assignment 4

## Members:
Alan McKay, Emma Runestad, Yuzuo Wang, Katie Michalski, and Meghan Nahnybida

This repository is for the collaboration and submission of Assignment 4 for
Introduction to networks and their Applications. The assignment involves the
implementation of various network intelligence services based on client
request. Two files are to be provided:

### cs3640-intelserver.py
Listens and sends via **TCP** from **port 5555 (127.0.0.1)** via a TCP socket
with its port and address bound to these values. TCP socket creation is as
follows:

> Creating a TCP socket with a call to `socket()`.

> Binding the socket to the listening port (`bind()`) after setting the port
number.

> Preparing the socket to listen for connections (making it a listening
socket), with a call to `listen()`

> Accepting incoming connections (`accept()`). This blocks the process until an
incoming connectin is received, and returns a socket descriptor for the
accepted connection. The initial descriptor remains a listening descriptor, and
`accept()` can be called again at any time with this socket, until closed.

> Communicating with the remote host with the API functions `send()` and
`recv()`, as well as with the general-purpose functions `write()` and `read()`.

>Closing each socket that was opened after use with the function `close()`

* This aglorithmic process is as described on Wikipedia:
[Client-server example using TCP](https://en.wikipedia.org/wiki/Berkeley_sockets#Client-server_example_using_TCP)
* Python implementation abstracts some of the detail out through the various
libraries involved. Previous assignment noted a [Socket Programming in Python](https://realpython.com/python-sockets/)
guide.
  + It may be worth looking into the **Handling Multiple Connections** section.
  It explores the limitations of socket programming that may have been enough
  to get by with the previous assignment; a ping and traceroute program.
  + [How to Work with TCP Sockets in Python (with Select Example)](https://steelkiwi.com/blog/working-tcp-sockets/)
  may be another good resource.


This file will implement the following functions to act as remote procedure
calls (RPC) for some client:

* `IPV4_ADDR(domain)`
  + The IPv4 address of `domain` specified in the command. Send an appropriate
and descriptive error message of required.
    - [CloudFlare's public DNS resolvers](https://www.cloudflare.com/learning/dns/what-is-1.1.1.1/)
    may be used for lookups.
    - The [dnspython](https://www.dnspython.org/about/) module may be used to
    resolve these DNS queries.
      * [Rdatatypes](https://dnspython.readthedocs.io/en/stable/rdatatype-list.html)
      * [Resolver Functions and The Default Resolver](https://dnspython.readthedocs.io/en/stable/resolver-functions.html)
      * [The dns.resolver.Resolver and dns.resolver.Answer Classes](https://dnspython.readthedocs.io/en/stable/resolver-class.html?highlight=dns.resolver.Resolver.resolve)
    - Other resources used:
      * [DNS Record Types Explained](https://phoenixnap.com/kb/dns-record-types)
* `IPV6_ADDR(domain)`
  + The IPv6 address of `domain` specified in the command. Send an appropriate
  and descriptive error message if required.
    - Cloudflare's public DNS resolvers and the dnspython module will once again
    be useful.
* `TLS_cert(domain)`
  + The TLS/SSL certificate associated with the `domain` specified in the
  command. Send an appropriate and descriptive error message if required.
    - Python's [ssl](https://docs.python.org/3/library/ssl.html#client-side-operation)
    may be useful.
    - Python's [getpeercert](https://docs.python.org/3/library/ssl.html#ssl.SSLSocket.getpeercert)
    may also be useful.
* `HOSTING_AS(domain)`
  + The name of the Autonomous System that hosts the IP address associated with
  the `domain` specified in the command. Sends an appropriate and descriptive
  error message if required.
    - The IP address associated with the domain will be needed. A WHOIS service
    to obtain information about the AS can then be used.
    [Team Cymru](https://team-cymru.com/community-services/ip-asn-mapping/#whois)'s
    WHOIS service can be used to accomplish this.
* `ORGANIZATION(domain)`
  + The name of the organization associated with the `domain` specified in the
  command. Send an appropriate error message if required.
    - The TLS certificate associated with the domain will be needed to parse
    out the subject's Organization Name from the certificate. the getpeercert
    method of the Python ssl module will be helpful here.

### cs3640-intelclient.py
Takes command line arguments from a user and constructs the appropriate client
commands, and parses/prints the server response. Client must be able to
successfully query the server on all listed functions. Mandatory arguments for
The Client to take:

* `intel_server_addr`
  + The address on which the Intel Server resides.
  + This is the address to which the client must establish a TCP connection.

* `intel_server_port`
  + The port on which the Intel Server is accepting queries.
  + This is the port on the address to which the client must establish a TCP
  connection.

* `domain`
  + The domain that is the subject of the query/
  + This is the domain that will be included in the message sent to the server.

* `service`
  + Must be one of the following: `IPV4_ADDR`, `IPV6_ADDR`, `TLS_CERT`,
  `HOSTING_AS`, or `ORGANIZATION`
  + This is the CLIENT COMMAND that will be included in the message sent to the
  server.

* Understanding of intelclient thus far:
  + Client sends a string to server in the packet data
  + Server reads the string and does appropriate operation
  + Server sends the string back to client

