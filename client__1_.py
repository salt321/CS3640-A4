import socket
import threading
import sys
import time

intel_server_addr = (sys.argv[1])
intel_server_port = int(sys.argv[2])
domain = sys.argv[3]
service = sys.argv[4]
#-----------args
message = domain + "$"+service
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((intel_server_addr, intel_server_port))


def receive():
    Stop = True
    info = ''
    while Stop:
        info = client.recv(1024).decode('utf-8')
        print(info)
        client.send(message.encode('utf-8'))
 
#use multi threading or 'main function to evoke receive'
reThread = threading.Thread(target= receive)
reThread.start()

