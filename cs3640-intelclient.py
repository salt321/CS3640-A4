import socket
import struct
import sys
import select

# understanding of intelclient thus far:
# - Client sends a string to server in the packet data
# - Server reads the string and does appropriate operation
# - Server sends the string back to client

# Checksum
def checksum(string):
    csum = 0
    countTo = (len(string) // 2) * 2
    count = 0
    answer = 0

    while count < countTo:
        thisVal = ord(string[count+1]) * 256 + ord(string[count])
        csum = csum + thisVal
        csum = csum & 0xffffffff
        count = count + 2
        if countTo < len(string):
            csum = csum + ord(string[len(string) - 1])
            csum = csum & 0xffffffff
            csum = (csum >> 16) + (csum & 0xffff)
            csum = csum + (csum >> 16)
            answer = ~csum
            answer = answer & 0xffff
            answer = answer >> 8 | (answer << 8 & 0xff00)
    print("Checksum: " + str(answer))
    return answer

# Create TCP a socket
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Define input arguments
intel_server_address = sys.argv[1]
intel_server_port = int(sys.argv[2])
domain = sys.argv[3]
service = sys.argv[4]

# Connect the socket to the port where the server is listening
server_address = (intel_server_address, intel_server_port)
mySocket.connect(server_address)

# Create a message to send to the server
# default basic values for packet
# unsure of this part
packet_id = 1111
header = struct.pack('bbHHh', 8, 0, 0, packet_id, 1111)
data = domain, service
new_checksum = checksum(str(header) + str(data))
header = struct.pack('bbHHh', 8, 0, socket.htons(new_checksum), packet_id, 1111)
data = struct.pack('f', data)
packet = header + data

# Send the message to the server
while packet:
    sent = mySocket.sendto(packet, (intel_server_address, intel_server_port))
    packet = packet[sent:]

# Receive the response from the server
while True:
    data = mySocket.recv(1024)
    # considering a pack was received
    if data:
        # not 100% on how to divide packet up
        header = data[:8]
        data = data[8:]
        type, code, checksum, packet_id, sequence = struct.unpack('bbHHh', header)
        print(data)
        mySocket.close()


# another option for receiving packets. we will need to play around once server is more developed to see what works
    # reply = select.select([mySocket], [], [], 3)
    # rec_packet, addr = mySocket.recvfrom(1024)
    # header = rec_packet[20:28]
    # type, code, checksum, packet_id, sequence = struct.unpack('bbHHh', header)
    # print("Packet id: ", packet_id)
