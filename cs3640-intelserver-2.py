import ssl
import socket
import subprocess
import dns.resolver
import json
import os


def IP_QUERY(domain,servers,IPv):
    resolver = dns.resolver.Resolver()

    if servers: #an empty list would convey 'use default'
        resolver.nameservers = servers

    # A float, the number of seconds to wait for a response from a server:
    resolver.timeout = 2

    # A float, the number of seconds to spend trying to get an answer to the...
    #  ...question.
    #If the lifetime expires a dns.exception.Timeout exception will be raised:
    resolver.lifetime = 2

    try:
        resolved = resolver.resolve(domain,IPv)
        return str(resolved[0])
    except dns.exception.Timeout:
        return "No acknowledgement from DNS server"
    except dns.resolver.LifetimeTimeout:
        return "DNS server has acknowledged, but a response was not generated"

def IPV4_ADDR(domain, servers = ['1.1.1.1']):
    return IP_QUERY(domain,servers,dns.rdatatype.A)


def IPV6_ADDR(domain,servers = ['1.1.1.1']):
    return IP_QUERY(domain,servers,dns.rdatatype.AAAA)

#get TLS/SSL cert
#Basically taken from python docs:
#https://docs.python.org/3/library/ssl.html#client-side-operation
#Not sure how this should look printed out, is very ugly by default
def TLS_CERT(domain):
    context = ssl.create_default_context()
    conn = context.wrap_socket(socket.socket(socket.AF_INET), server_hostname=domain)
    conn.connect((domain, 443))
    return conn.getpeercert()


#get name of AS hosting IP address associated w domain
#Not confident my output is what we're looking for but this does get a response from the whois thing, so changing it should be easy if need be
#Unfortunately the AS isn't always in the same place, so have to search. Maybe a better way to do it, this was just easy
#Also not sure if there's a better way to get info out of the subprocess than making it a string
#using subprocess to execute command
#https://docs.python.org/3/library/subprocess.html
#https://www.team-cymru.com/ip-asn-mapping#whois
def HOSTING_AS(domain):
    print(IPV4_ADDR(domain))
    result = subprocess.run(["whois", IPV4_ADDR(domain)], capture_output=True)
    resultStr = str(result)
    resultSplit = resultStr.split("\\n")
    
    AS = None
    for i in resultSplit:
        if "OriginAS" in i:
            AS = i
    return AS

#name of the organization associated w domain
#Unfortunately this isn't always located in the same position in the certificate so searching is necessary. Probably could be done nicer than I did though
#cert is a dictionary, issuer is a tuple containing several layers of tuples
#Not 100% positive this is what they're asking us to get from the cert but it should be pretty trivial to fix if not
def ORGANIZATION(domain):
    cert = TLS_CERT(domain)
    issuer = cert.get("issuer") 
    i = 0
    org = None
    while i < len(issuer): 
        if issuer[i][0][0] == "organizationName":
            org = issuer[i][0][1]
            i = len(issuer)
        i += 1
    if org == None: #Should perhaps be a real error and not just a printout, but IDK
        return "Organization not found"
    else:
        return org

# ----- ----- ----- #
# The above functions act as remote procedure calls for some client. What needs
# to occur now is to allow the server to listen to some port and respond to any
# requests for these procedures.

#------------------------The real server part

host = '127.0.0.1'
port = 5555
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((host,port)) # you can change the server address to whatever you want. here, I just use "self" as the address
server.listen()

def receive():
    domain = ''
    service = ''

    while True:
        print("The server is listening")
        client,address = server.accept()
        print(f'connection is established with {str(address)}')
        client.send('connection established'.encode('utf-8')) 
        message = client.recv(1024)
        message = message.decode('utf-8')
        #print(message.decode('utf-8'))
        for i in range (len(message)):
        	if (message[i] == '$'):
        		domain = message[:i]
        		service = message[i+1:]
        		#seperate domain and service
        		
        if (service == 'IPV4_ADDR'):
        	result = IPV4_ADDR(domain)
        	mess = 'the IPV4 address of the domain is:'
        	client.send(mess.encode('utf-8')+result.encode('utf-8'))
        	

        elif (service == 'IPV6_ADDR'):
        	result = IPV6_ADDR(domain)
        	mess = 'the IPV6 address of the domain is:'
        	client.send(mess.encode('utf-8')+result.encode('utf-8'))
        elif (service == 'TLS_CERT'):
        	result = TLS_CERT(domain)
        	result = json.dumps(result)
        	mess = 'The TLS/SSL certificate associated with the domain is:'
        	client.send(mess.encode('utf-8')+result.encode('utf-8'))
        elif (service == 'HOSTING_AS'):
        	result = HOSTING_AS(domain)
        	client.send(result.encode('utf-8'))
        elif (service == 'ORGANIZATION'):
        	result = ORGANIZATION(domain)
        	client.send(result.encode('utf-8'))
        #maybe need a error output here// use try ... except...
if __name__ == "__main__":
    receive()
	


